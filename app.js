var http = require('http');
var fs = require('fs');
var url = require('url');
var execSync = require('child_process').execSync;
const request = require('request');

// gpio readall
// Надо номера пинов в колонке BCM
var app = {
	pins: [], // index = pin BCM, значение = 1/0 - состояние. По умолчанию значение null или  -1 значит не установлено.
	pinEvents: [] // index = pin BCM, значение = url 
}
var on    = 1;
var off   = 0;
var unset = -1;
var appStateFile = "pins_state.json";
var header = '';
var footer = '</pre></body></html>';

//
// Convenient date format for logging
function getDateTime() {
	var date = new Date();

	var hour = date.getHours();
	hour = (hour < 10 ? "0" : "") + hour;

	var min  = date.getMinutes();
	min = (min < 10 ? "0" : "") + min;

	var sec  = date.getSeconds();
	sec = (sec < 10 ? "0" : "") + sec;

	var year = date.getFullYear();

	var month = date.getMonth() + 1;
	month = (month < 10 ? "0" : "") + month;

	var day  = date.getDate();
	day = (day < 10 ? "0" : "") + day;

	return year + ":" + month + ":" + day + " " + hour + ":" + min + ":" + sec;
}

function log( txt ) {
	var time = getDateTime();
	console.log(time + ' ' + txt);
}

function digitalRead(pin)
{
	var output = fs.readFileSync('/sys/class/gpio/gpio' + pin + '/value', "utf8");
	var values = output.split(":");
	var result = Number(values[values.length - 1]);
	//console.log(result);
	return result
}

function digitalWrite(pin, value) {
	pin_path = '/sys/class/gpio/gpio' + pin + '/value';
	if( ! fs.existsSync( pin_path ) ) {
		pinMode( pin, 'out' );
	}
	fs.writeFileSync( pin_path, String(value) );
}

function pinMode(pin, mode) {
	try {
		// it's ok to receive error here - it means we already started script before and file exists
		fs.writeFileSync('/sys/class/gpio/export', String(pin));
	} catch(err) {
		log(err);
	}
	try {
		// error here usually happens when run not under root
		// TODO learn and setup wiringPi to be able to run as pi user
		fs.writeFileSync('/sys/class/gpio/gpio' + pin + '/direction', String(mode)); // mode = out, in
	} catch(err) {
		log(err);
		log('OMG use sudo !!!!!!!!!!');
		log('sudo node app.js');
		process.exit(code=1)
	}
}

function delay(ms) {
	var start = new Date().getTime();
	while(1) {
		if ((new Date().getTime() - start) > ms) {
			break;
		}
	}
}

// Send file content to client
function sendFile( res, file) {
	//  fs.readFile( __dirname + file, 'utf-8', function(err, content) {
	fs.readFile( __dirname + file, function(err, content) {
		if (!err) {
			res.end( content );
		} else {
			log( 'Error, reading file: ' + err );
			res.end( 'Error, reading file ' + file + '. Check log' );
		}
	});
}


function sendGPIOReadAll(res) {
	//gpio = execSync('gpio readall').toString().replace(/\n/g, '<br>').replace(/ /g, '&nbsp;');
	header = fs.readFileSync(  __dirname + '/header.html' ).toString();
	gpio = execSync('gpio readall').toString();
	res.end( header + gpio + footer );
}

function savePins() {
	fs.writeFile( __dirname + '/' + appStateFile, JSON.stringify(app), function(err) {
		if(err) {
			log(err);
			log("Error, saving app setting to filename:" + __dirname + '/' + appStateFile);
		}
	});
}

function loadPins() {
	fs.readFile( __dirname + '/' + appStateFile, function(err, data) {
		if(err) {
			log(err);
			log("Error, reading settings from filename:" + __dirname + '/' + appStateFile);
		} else {
			if(data.indexOf('pins') == -1) {
				app.pins = JSON.parse(data); // TODO: remove it in future - this is old api
			} else {
				app = JSON.parse(data);
			}
			log('Successfully restored app state:');
			log(app);
			for (pin in app.pins.length) {
				if( app.pins[pin] != null && app.pins[pin] != -1 ) {
					pinMode( pin, 'out' );
					digitalWrite( pin, app.pins[pin] );
				}
			}
			for(pin in app.pinEvents ) {
				if(app.pinEvents[pin] !== null && app.pinEvents[pin].length > 0 ) {
					pinMode( pin, 'in' );
				}
			}
		}
	})
}


log( 'Application started! Waiting for server to start.' );
log( 'Directory: ' + __dirname );

// Loading the index file . html displayed to the client
var server = http.createServer(function(req, res) {
	var parsedURL = url.parse(req.url, true);
	var path = parsedURL.pathname;
	var query = parsedURL.query;
	switch (path) {
		case '/':
		case '/index.html':
			res.writeHead(200, {'Content-Type': 'text/html'});
			//sendFile( res, '/index.html' );
			sendGPIOReadAll(res);
			break;
		case '/favicon.ico':
			res.writeHead(200, {'Content-Type': 'image/x-icon'});
			sendFile( res, path );
			break;
		case '/jquery.min.js':
			res.writeHead(200, {'Content-Type': 'text/javascript'});
			sendFile( res, path );
			break;
	default:
			api = req.url.split('/'); // req.url contains full path with parameters after ?    path is only path with out ? parameter
			if( (api.length > 1) && (api[1] == 'pin') ) {
				if( (api.length > 3) && (api[3].length > 0) ) {
					pin = parseInt(api[2]);
					if(isNaN(pin) || pin > 27) { // BCM max pin number is 27
						res.writeHead(422, {'Content-Type': 'text/plain'}); // 422 (Unprocessable Entity) status code
						res.end( 'Wrong value for pin Number. Supplied path was: ' +path );
						return;
					}
					if( api.length > 4 && api[3] == 'onchange' ) {
						if(api[4].length == 0) {
							app.pinEvents[pin] = null;
							log('delete event for pin ' + pin);
						} else {
							app.pinEvents[pin] = api.slice(4).join('/'); // url
							log('add event for pin ' + pin + ' url: ' + app.pinEvents[pin]);
							pinMode( pin, 'in' );
						}
						res.end( JSON.stringify({pin: pin, onchange: app.pinEvents[pin]}) );
						savePins();
						return;
					} else {
						value = parseInt(api[3]);
						log('call api set pin: ' + pin + ' value: ' + value);
						if(isNaN(value)) {
							res.writeHead(422, {'Content-Type': 'text/plain'}); // 422 (Unprocessable Entity) status code
							res.end( 'Wrong value for pin State. Supplied path was: ' +path );
							return;
						}

						if( app.pins[pin] == unset ) {
							pinMode( pin, 'out' );
						}
						app.pins[pin] = value;
						digitalWrite( pin, value );
						savePins();
					}
				}
				res.end( JSON.stringify(app.pins) );
			} else {
				res.writeHead(404, {'Content-Type': 'text/plain'});
				res.end('404 Not found');
				log ('No case for:' + path);
			}
			break;
	}
	log(path);
});

loadPins();


server.on('error', function(err) {
	log(err);
	log('Some one is already listening on 80 port, find and kill it!');
	log('sudo netstat -nlp | grep 80');
	process.exit(code=1);
});

server.listen(80);

log('Server listening on port 80');

function loop() {
	for(pin in app.pinEvents ) {
		if(app.pinEvents[pin] !== null && app.pinEvents[pin].length > 0 ) {
			var state = digitalRead(pin);
			if(app.pins[pin] != state) {
				log('Pin ' + pin + ' state changed. New state: ' + state);
				app.pins[pin] = state;
				savePins();
				request(app.pinEvents[pin].replace('api_value',state),  { timeout: 1500, json: true }, function (error, res, body) {
					if (!error && res.statusCode == 200) {
						log(body);
					} else {
						log(error);
					}
				});
			}
		}
	}
}
// Checking if some-one press real switch (button) in the garage.
setInterval(loop, 100);
